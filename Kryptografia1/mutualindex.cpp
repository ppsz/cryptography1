#include <iostream>
#include <string>
#include <fstream>
#include <limits>
#include <cmath>
#include <iomanip>
#include "vigenere.h"

bool mutualAgreementIndex(std::string input_file_name, int number_of_groups, bool automation)
{
	std::ifstream input_file;
	std::string encrypted_word;
	int number_of_letters[10][26] = { 0 };
	int shift[10][10] = { 0 };
	int best_index_match[10][10] = { 0 };
	float size_of_group[10] = { 0 };
	float mutual_index[10][10][26] = { 0 };
	std::string test_string[10];

	//---Wczytanie pliku---
	input_file.open(input_file_name);
	if (!input_file.is_open()) return 0;
	input_file >> encrypted_word;
	

	//---Liczenie liter w grupach---
	for (int i = 0; i < number_of_groups; ++i)
	{
		for (int j = i; j < encrypted_word.length(); j += number_of_groups)
		{
			for (int k = 0; k < 26; ++k)
			{
				if (static_cast<int>(encrypted_word[j]) == (k + 97))
				{
					++number_of_letters[i][k]; 
					test_string[i] += encrypted_word[j];
					++size_of_group[i];
				}
			}
		}
	}
	//for (int i = 0; i < number_of_groups; ++i)
	//{
	//	std::cout << "grupa numer: " << i << "\n";
	//	//for (int j = i; j < 26; ++j)
	//	//{
	//		//std::cout << static_cast<char>(j + 97) << ": " << number_of_letters[i][j] << "\n";
	//		std::cout << test_string[i] << "\n";
	//	//}
	//}


	//---liczenie indeksu zgodnosci miedzy grupami wraz z przesunięciem---
	for (int i = 0; i < number_of_groups; ++i)
	{
		for (int j = i; j < number_of_groups - 1; ++j)
		{
			for (int k = 0; k < 26; ++k)
			{
				for (int n = 0; n < 26; ++n)
				{
					mutual_index[i][j][k] += (number_of_letters[i][n] * number_of_letters[j + 1][((n + k) % 26)]);
				}
				mutual_index[i][j][k] /= size_of_group[i] * size_of_group[j + 1];
				//---Szukanie elementu najbardziej zblizonego do 0.065---
				if (std::abs(mutual_index[i][j][best_index_match[i][j]] - 0.065) > std::abs(mutual_index[i][j][k] - 0.065))
					best_index_match[i][j] = k;
			}
		}
	}
	if (automation == true)
	{
		for (int i = 0; i < number_of_groups; ++i)
			for (int j = i; j < number_of_groups - 1; ++j)
				for (int k = 0; k < 26; ++k)
					if (best_index_match[i][j] == k)
						shift[i][j] = k;
	}
	else
	{
		//---wypisanie indeksów zgodnosci---
		for (int i = 0; i < number_of_groups; ++i)
		{
			for (int j = i; j < number_of_groups - 1; ++j)
			{
				std::cout << "Indeks zgodnosci miedzy k" << i << " a k" << j + 1 << " dla kolejnych przesuniec wynosi:\n";
				for (int k = 0; k < 26; ++k)
				{
					std::cout << std::setprecision(2) << std::right
						<< std::setw(3) << k << " : "
						<< std::setw(5) << std::showpoint << std::left << mutual_index[i][j][k] << " | roznica: "
						<< std::setw(5) << std::abs((mutual_index[i][j][k] - 0.065));
					//---oznaczenie elementu najbardziej zblizonego do 0.065---
					if (best_index_match[i][j] == k)
						std::cout << " <---Najmniejszy element (nr " << k << ")---\n";
					else
						std::cout << "\n";
				}
				shift[i][j] = getValue("Ktora wartosc przesuniecia wybrac? ", 0, 25, false);
			}
		}
	}
	//---Wypisanie podstawowych wartosci klucza---
	std::cout << "Klucz moze byc postaci:\nK0";
	for (int i = 0; i < number_of_groups - 1; ++i)
		std::cout << ",K" << i + 1 << "=K0+" << shift[0][i];

	//---Wypisanie dodatkowych wartosci klucza---
	std::cout << "\nInne zaleznosci:\n";
	for (int i = 1; i < number_of_groups - 1; ++i)
	{
		std::cout << "K" << i;
		for (int j = i + 1; j < number_of_groups; ++j)
		{
			std::cout << ",K" << j << "=K" << i << "+" << shift[i][j - 1];
		}
		std::cout << "\n";

	}
	return 0;
}