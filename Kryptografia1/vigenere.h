#pragma once

#include <string>

//---Menu glowne---
bool mainMenu();

//---odczyt z pliku -> szyfrowanie -> zapis do pliku---
bool encryptFile(std::string encription_key, std::string input_file_name, std::string output_file_name);

//---liczenie indeks�w zgodno�ci w p�tli (do 10 grup) w celu znalezienia d�ugo�ci klucza---
int keySizeCheckAgreementIndex(std::string input_file_name, bool automation);

//---liczenie wzajemnego indeksu zgodno�ci grup---
bool mutualAgreementIndex(std::string input_file_name, int number_of_groups, bool automation);

//---pobieranie wartosci calkowitej (zabezpieczenie przed wprowadzeniem blednych danych)---
int getValue(std::string message_to_print, int min, int max, bool clear_after_mistake);