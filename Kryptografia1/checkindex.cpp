#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "vigenere.h"

int keySizeCheckAgreementIndex(std::string input_file_name, bool automation)
{
	std::ifstream input_file;
	std::string encrypted_word;
	int number_of_groups;
	int best_index_match = 0;
	int number_of_letters[10][10][26] = { 0 };
	float size_of_group;
	float average_agreement_index[10] = { 0 };
	float agreement_index[10][10] = { 0 };


	//---Wczytanie ilosci grup---
	if(automation==true)
		number_of_groups = 10;
	else
		number_of_groups = getValue("Podaj gorny zakres, do ktorego maja byc sprawdzane indeksy grup: ", 1, 10, true);

	//---Wczytanie pliku---
	input_file.open(input_file_name);
	if (!input_file.is_open()) return 0;
	input_file >> encrypted_word;

	//---Liczenie liter w kazdej z grup--- 
	for (int i = 0; i < number_of_groups; ++i)
		for (int j = 0; j <= i; ++j)
			for (int k = j; k < encrypted_word.length(); k += i+1)
				for (int m = 0; m < 26; ++m)
					if (static_cast<int>(encrypted_word[k]) == (m + 97))
						++number_of_letters[i][j][m];

	//---Liczenie indeksu zgodnosci---
	for (int i = 0; i < number_of_groups; ++i)
	{
		for (int j = 0; j <= i; ++j)
		{
			size_of_group = 0;
			for (int k = 0; k < 26; ++k)
			{
				agreement_index[i][j] += (number_of_letters[i][j][k] * (number_of_letters[i][j][k] - 1));
				size_of_group += number_of_letters[i][j][k];
			}
			agreement_index[i][j] /= (size_of_group*(size_of_group - 1));
			//---liczenie sredniej wartosci---
			average_agreement_index[i] += agreement_index[i][j];
		}
		average_agreement_index[i] /= i+1;
		//---szukanie indeksu najmniejszej wartosci---num = floor( num * 100.00 + 0.5 ) / 100.00;
		if ((std::floor((std::abs(average_agreement_index[best_index_match] - 0.065)*1000.00 + 0.5)) / 1000.00)
			> (std::floor((std::abs(average_agreement_index[i] - 0.065)*1000.00 + 0.5)) / 1000.00))
		{
			best_index_match = i;
		}
	}

	if (automation == true)
	{
		return best_index_match+1;
	}
	else
	{
		//---Wypisanie indeksu zgodnosci dla tylko 1 grupy (klucz jednoliterowy)---
		std::cout << "Liczba grup: 1\n I0 = " << std::setprecision(2) << std::showpoint << agreement_index[0][0];
		if (best_index_match == 0)
			std::cout << " <---Najblizszy wynik (nr 0)---\n--------------------\n";
		else
			std::cout << "\n--------------------\n";
		//---Petla do wypisania indeksu zgodnosci dla wiecej niz 1 grupy
		for (int i = 1; i < number_of_groups; ++i)
		{
			std::cout << "Liczba grup: " << i + 1 << "\n";
			for (int j = 0; j <= i; ++j)
			{
				std::cout << std::setprecision(2) << std::left
					<< " I" << std::setw(2) << j + 1 << "= " << std::setw(5) << std::showpoint
					<< agreement_index[i][j] << "\n";
			}
			std::cout << "  ~I" << std::setprecision(2) << std::setw(5) << std::showpoint << average_agreement_index[i];
			//---oznaczenie elementu najbardziej zblizonego do 0.065---
			if (best_index_match == i)
				std::cout << " <---Najblizszy sredni wynik (nr " << i + 1 << ")---\n";
			else
				std::cout << "\n";
			std::cout << "--------------------\n";
		}

	}
	return 0;
}