#include <string>
#include <fstream>
#include "vigenere.h"

bool encryptFile(std::string encription_key, std::string input_file_name, std::string output_file_name)
{
	std::ifstream input_file;
	std::ofstream output_file;
	std::string word, encrypted_word;

	//---Wczytanie pliku---
	input_file.open(input_file_name);
	if (!input_file.is_open()) return false;
	while (input_file >> word)
		encrypted_word += word;
	input_file.close();

	//---Szyfrowanie---
	for (int i = 0; i < encrypted_word.length(); ++i)
		encrypted_word[i] = ((encrypted_word[i] - 97 + encription_key[i % encription_key.length()] - 97) % 26) + 97;

	//---Zapis do pliku---
	output_file.open(output_file_name);
	if (!output_file.is_open()) return false;
	output_file << encrypted_word;
	output_file.close();

	return true;
}