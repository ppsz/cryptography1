#include <iostream>
#include <iomanip>
#include <fstream>
#include "vigenere.h"


bool mainMenu()
{
	std::ifstream key_file;
	std::string encription_key;
	std::string input_file_name = "input.txt";
	std::string output_file_name = "output.txt";
	int menu_select, number_of_groups;

	//---Wczytanie pliku---
	key_file.open("key.txt");
	if (!key_file.is_open()) return 0;
	key_file >> encription_key;

	//---Petla menu glownego
	for (;;)
	{
		std::cout
			<< "|---------------------------------|\n"
			<< "|           Menu Glowne           |\n"
			<< "|---------------------------------|\n"
			<< "| 1: Zaszyfruj plik input.txt     |\n"
			<< "|     (do pliku output.txt)       |\n"
			<< "| Odszyfruj haslo:                |\n"
			<< "| 2: Manualnie                    |\n"
			<< "| 3: Automatycznie                |\n"
			<< "|                                 |\n"
			<< "| 0: Wyjdz z programu             |\n"
			<< "|---------------------------------|\n";
		
		menu_select = getValue("   Wybierz pozycje menu: ", 0, 3, false);

		switch (menu_select)
		{
		case 1:
			if (encryptFile(encription_key, input_file_name, output_file_name))
			{
				std::cout
					<< "|---------------------------------|\n"
					<< "|      Szyfrowanie ukonczone      |\n"
					<< "|    uzyty klucz to "
					<< std::setw(10) << std::left << encription_key
					<< "    |\n";
			}
			else
			{
				std::cout
					<< "|---------------------------------|\n"
					<< "|Wystapil blad podczas szyfrowania|\n";
			}
			break;
		case 2:
			keySizeCheckAgreementIndex(output_file_name, false);
			number_of_groups = getValue("Podaj ilosc grup do policzenia wzajemnego indeksu zgodnosci: ", 2, 10, false);
			mutualAgreementIndex(output_file_name, number_of_groups, false);
			break;
		case 3:
			number_of_groups = keySizeCheckAgreementIndex(output_file_name, true);
			mutualAgreementIndex(output_file_name, number_of_groups, true);
			break;
		case 0:
			return true;
			break;
		default:
			return false;
		}
	}

	return true;
}